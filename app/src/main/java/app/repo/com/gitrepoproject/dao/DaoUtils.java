package app.repo.com.gitrepoproject.dao;

import java.util.List;

import io.realm.Realm;

public class DaoUtils {
    public static List<Bookmark> getBookmarks() {
        Realm realm = Realm.getDefaultInstance();
        List<Bookmark> result = realm.where(Bookmark.class).findAll();
        realm.close();
        return result;
    }

    public static void addToBookmark(Long id) {
        Realm realm = Realm.getDefaultInstance();
        final Bookmark bookmark = new Bookmark(id);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(bookmark);
            }
        });
        realm.close();
    }

    public static void removeFromBookmarks(final long id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Bookmark bookmark = realm.where(Bookmark.class).equalTo("id", id).findFirst();
                bookmark.deleteFromRealm();
            }
        });
        realm.close();
    }
}
